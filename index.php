<!DOCTYPE HTML>
<html>
<title>Ajax table pagination with MySQL, PHP and jQuery - InfoTuts</title>
<head>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css" />

</head>
<body>

<div id="pagination" cellspacing="0"></div>

<script type="text/javascript">
	
 $(function(){
 $.ajax({
	     url:"dbmanupulate.php",
                  type:"POST",
                  data:"actionfunction=showData&page=1",
        cache: false,
        success: function(response){
		   
		  $('#pagination').html(response);
		 
		}
		
	   });
    $('#pagination').on('click','.page-numbers',function(){
       $page = $(this).attr('href');
	   $pageind = $page.indexOf('page=');
	   $page = $page.substring(($pageind+5));
       
	   $.ajax({
	     url:"dbmanupulate.php",
                  type:"POST",
                  data:"actionfunction=showData&page="+$page,
        cache: false,
        success: function(response){
		   
		  $('#pagination').html(response);
		 
		}
		
	   });
	return false;
	});
	
});
	   

</script>
</body>
</html>